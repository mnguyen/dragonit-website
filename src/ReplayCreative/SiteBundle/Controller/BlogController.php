<?php

namespace ReplayCreative\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Post controller.
 *
 */
class BlogController extends Controller
{
    /**
     * Lists all Post entities.
     *
     */
    public function indexAction()
    {
        return $this->redirect('/');
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('ReplayCreativeBlogBundle:Post')->findByIsActive(true);

        return $this->render('ReplayCreativeSiteBundle:Blog:index.html.twig', array(
            'posts' => $posts,
        ));
    }

    /**
     * Finds and displays a Post entity.
     *
     */
    public function showAction($id, $slug)
    {
        return $this->redirect('/');
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('ReplayCreativeBlogBundle:Post')->find($id);
       


        if (!$post) {
            throw $this->createNotFoundException('Unable to find Post.');
        }


        return $this->render('ReplayCreativeSiteBundle:Blog:post.html.twig', array(
            'post'      => $post,
        ));
    }

    public function tagsComponentAction()
    {
       // $tags = $this->getDoctrine()->getManager()->getRepository('ReplayCreativeBlogBundle:Tag')->findAll();
        $tags = array();
        return $this->render('ReplayCreativeSiteBundle:Blog:tags.html.twig', array(
            'tags'      => $tags,
        ));
    }

}
