var contact_us_valid = false;
jQuery(document).ready(function() {
    jQuery("#contactx-form").submit(function(){
        path_url = jQuery(this).attr("action");
		
        if(!window.contact_us_valid){	
            jQuery.ajax({
                url: path_url,
                async: true,
                dataType: "json",
                data: jQuery(this).serialize(),
                type: 'POST',
                success: function(response){
					
                    if(response && response.valid)
                    {
                        window.contact_us_valid = true;
                        jQuery("#contact-us").trigger("submit");
                    }
	              
                    if(response && response.fields)
                    {  
                        jQuery(".contact_input").each(function(index, value){  
                            jQuery(this).removeClass("input_error");
                        });
	            	  
                        jQuery.each(response.fields, function( index, value ) {
                            jQuery("#" + value).addClass("input_error");
                            jQuery("#" + value).attr("title", response.messages[value] );
                        });
                    }
                }
            });
			return false;
		}	
	});

});