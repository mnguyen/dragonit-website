angular.module('rc', [])
 
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[').endSymbol(']}');
})
 
.controller('PageCtrl', function($scope, $timeout, $log, $anchorScroll, $location) {
  
  $scope.state_what_we_do = function() {
    var frame = 1;
    var verbs = ['', 'create', 'build', 'analyze', 'ideate', 'make', 'program', 'develop', 'strategize', 'market'];
    var addFrame = function(label, timeout) {
      $timeout(function() {
          $scope.label = label;
      }, timeout);
    }; 
    var t = (verbs.length) * 1000;
    /* $timeout(function() {
        $('a[href="#about"]').first().click();
    }, t);
    */
    for (var x=0; x < verbs.length; x++) {
      addFrame(verbs[x], t);
      t = t - 1000;
    }
  };
  setTimeout(function() {
    $scope.state_what_we_do();
  }, 2000);
});
