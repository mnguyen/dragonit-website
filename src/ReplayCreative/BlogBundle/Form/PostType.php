<?php

namespace ReplayCreative\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('body', 'textarea')
            ->add('shortDescription', 'textarea', array('label' => "Short Description"))
            ->add('slug')
            ->add('tags', 'entity', array('class' => 'ReplayCreativeBlogBundle:Tag', 'multiple' => true, 'expanded' => true, 'property' => 'name'))
            ->add('image', 'file', array('required' => false, 'data_class' => null))
            ->add('isActive')
            ->add('author')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ReplayCreative\BlogBundle\Entity\Post'
        ));
    }

    public function getName()
    {
        return 'replaycreative_blogbundle_posttype';
    }
}
